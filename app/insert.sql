INSERT INTO habi_db.property (address,city,price,description,`year`) VALUES
	 ('calle 23 #45-67q','bogota',120000000,'Hermoso apartamento en el centro de la ciudad',2000),
	 ('carrera 100 #15-90w','bogota',350000000,'Amplio apartamento en conjunto cerrado',2011),
	 ('diagonal 23 #28-21e','bogota',270000000,'Apartamento con hermosas vistas',2018),
	 ('calle 23 #45-67r','medellin',210000000,'',2002),
	 ('carrera 100 #15-90e','medellin',325000000,'Amplio apartamento en conjunto cerrado',2011),
	 ('diagonal 23 #28-21s','medellin',270000000,'',NULL),
	 ('carrera 100 #15-90x','barranquilla',35000000,NULL,2015),
	 ('carrera 22 #34-96v','manizales',39483059,NULL,1800),
	 ('','',0,NULL,NULL),
	 (NULL,NULL,0,NULL,NULL),
	 (NULL,'',0,NULL,NULL),
	 ('','',0,NULL,NULL),
	 ('','',0,NULL,NULL),
	 (NULL,'',0,NULL,NULL),
	 ('calle 95 # 78 - 123','bogota',120000000,'hermoso acabado, listo para estrenar',2020);
INSERT INTO habi_db.property (address,city,price,description,`year`) VALUES
	 ('calle 18 k 43','cali',125000000,NULL,NULL),
	 ('Cll 1A #11B','pereira',300000000,'hermoso acabado, listo para estrenar super comodo',2020),
	 ('Cll 1A #20b','pereira',300000000,'hermoso acabado, listo para estrenar super comodo',2020),
	 ('Malabar 2v','pereira',350000000,'Casa campestre con hermosos paisajes',2020),
	 ('casa 24c','pereira',450000000,'Casa campestre con sala de lujo tecnologica',2020),
	 ('via cerritos','pereira',250000000,'Full casa amoblada',2020),
	 ('5 via cerritos','pereira',270000000,'Casa campestre con lago',2020),
	 ('C6 Panorama','dosquebradas',290000000,'Casa con entrada al centro comercial',2020),
	 ('C67 Umbras','belen de umbria',120000000,'Casa con entrada al centro comercial',2020);

INSERT INTO habi_db.property (address,city,price,description,`year`) VALUES
	 ('Cll 1A #11B-123','pereira',300000000,'hermoso acabado, listo para estrenar super comodo',2016),
	 ('Cll 1A #11B-234','pereira',300000000,'hermoso acabado, listo para estrenar super comodo',2019),
	 ('Malabar entrada 345','pereira',350000000,'Casa campestre con hermosos paisajes',2021),
	 ('Maracay casa 567c','pereira',450000000,'Casa campestre con sala de lujo tecnologica',2020),
	 ('Entrada 8 via cerritos','pereira',250000000,'Full casa amoblada',2020),
	 ('Entrada 9 via cerritos','pereira',270000000,'Casa campestre con lago',2021),
	 ('M8 C634 Panorama','dosquebradas',290000000,'Casa con entrada al centro comercial',2017),
	 ('Bloque 53 C674 Umbras','belen de umbria',120000000,'Casa con entrada al centro comercial',2000);
    

85
86
87
88
89
90
91
92
93




INSERT INTO habi_db.status_history (property_id,status_id,update_date) VALUES
	 (94,4,'2021-04-10 14:25:56'),
	 (58,3,'2021-04-10 15:26:56'),
	 (59,5,'2021-04-10 16:34:56'),
	 (60,3,'2021-04-10 17:23:56'),
	 (61,3,'2021-04-10 18:23:56')
INSERT INTO habi_db.status_history (property_id,status_id,update_date) VALUES
	 (85,3,'2021-04-10 20:23:56'),
	 (86,5,'2021-04-10 21:12:56'),
	 (87,3,'2021-04-10 22:23:56'),
	 (88,5,'2021-04-10 23:23:56'),
	 (89,3,'2021-04-10 22:23:56'),
	 (90,3,'2021-04-10 09:23:56'),
	 (91,3,'2021-04-10 10:23:56'),
	 (92,5,'2021-04-10 11:23:56'),
	 (93,3,'2021-04-10 12:23:56'),
INSERT INTO habi_db.status_history (property_id,status_id,update_date) VALUES
	 (74,3,'2021-04-10 07:23:56'),
	 (75,3,'2021-04-10 06:23:56'),
	 (76,3,'2021-04-10 05:23:56'),
	 (77,5,'2021-04-10 04:23:56'),
	 (78,3,'2021-04-10 03:23:56'),
	 (79,5,'2021-04-10 05:23:56'),
	 (80,3,'2021-04-10 02:23:56'),
	 (81,4,'2021-04-10 01:23:56'),
	 (82,3,'2021-04-10 22:23:56'),
	 (83,5,'2021-04-10 22:23:56'),
	 (84,3,'2021-04-10 22:23:56')

