from typing import List, Optional
from pydantic import BaseModel
"""
Aqui se establecen los schemas de la respuesta
del endpoint, definiendo las keys del json y 
tambien el tipo de dato a enviar
"""


class StatusProperty(BaseModel):
    id: int
    name: str
    label: str
    update: str


class Property(BaseModel):
    id: int
    address: str
    city: str
    price: int
    description: Optional[str] = None
    year_built: int
    status: StatusProperty


class ResponseProperty(BaseModel):
    error: Optional[bool] = False
    message: Optional[str] = None
    total_rows: int = 0
    response: List[Property]
