from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from sqlalchemy.sql import text
import configparser

config = configparser.ConfigParser()
config.read('.env')
env = config['CONFIGURATION']

# Se realiza la conexion a la base de datos con parametros servidos
# por configparse
CONN_STRING = "mysql://{user}:{password}@{host}:{port}/{db}".format(
    user=env['DB_USER'],
    password=env['DB_PASSWORD'],
    host=env['DB_HOST'],
    port=env['DB_PORT'],
    db=env['DB_DATABASE'])

# Se establece la conexion a la base de datos
engine = create_engine(CONN_STRING, connect_args={})
session = sessionmaker(autocommit=False, autoflush=False, bind=engine)
Base = declarative_base()


class db:
    """
    Esta clase se creo con el fin de servir las funciones de consulta,
    filtrado y formateado de datos.
    """
    def __init__(self) -> None:
        self.conn = engine.connect()

    def parse_status(self, status) -> list:
        """
        Se crea una lista de status para el filtro
        con base a los parametros en la url
        """
        if status == '*':
            return []
        else:
            dataSplit = status.split(',')
            if len(dataSplit) <= 1:
                data = ['s.name IN ("{}")'.format(dataSplit[0])]
            else:
                data = ['s.name IN {}'.format(tuple(map(str, dataSplit)))]
            return data

    def parse_years(self, year_built: str):
        # crea una lista de años para el filtro
        # con base a los parametros en la url
        if year_built:
            dataSplit = year_built.split(',')
            if len(dataSplit) <= 1:
                data = ['p.`year` IN ("{}")'.format(dataSplit[0])]
            else:
                data = ['p.`year` IN {}'.format(tuple(map(str, dataSplit)))]
            return data
        else:
            return []

    def parse_city(self, city: str):
        # crea una lista de ciudades para el filtro
        # con base a los parametros en la url
        if city:
            dataSplit = city.split(',')
            if len(dataSplit) <= 1:
                data = ['p.`city` IN ("{}")'.format(dataSplit[0])]
            else:
                data = ['p.`city` IN {}'.format(tuple(map(str, dataSplit)))]
            return data
        else:
            return []

    def where(self, params: str) -> str:
        # crea el string de todos los parametros o filtros
        filters = ["INSTR(`address`, ' ') > 0"]
        if params:
            filters = filters + params
        result = ' AND '.join(filters)
        return result

    def setup_filter(self, year_built: str, city: str, params: str):
        params = self.parse_status(params)
        years = self.parse_years(year_built)
        city = self.parse_city(city)

        return params + years + city

    def filter(self, year_built: str, city: str, params: str, size: int,
               offset: int) -> list:
        # se crea el string con el query SQL
        # se utiliza desc para mostrar lo mas reciente
        query = """
                    SELECT
                        p.id as property_id,
                        p.address as property_address,
                        p.city as property_city,
                        p.price as property_price,
                        p.description as property_description,
                        p.`year` as property_year,
                        s.*,
                        sh.update_date as status_update_date
                    FROM
                        property p
                    JOIN (
                        SELECT  sh.property_id as property_id,
                                sh.status_id as status_id,
                                MAX(sh.update_date) as update_date
                        FROM    status_history sh
                        GROUP BY property_id
                        ORDER BY status_id
                    ) sh on (sh.property_id = p.id)

                    INNER JOIN
                        status s
                    ON
                        s.id = sh.status_id AND s.id > 2
                    WHERE
                        {where}
                    ORDER BY
                        p.id desc
                    LIMIT
                        {limit}
                    OFFSET
                        {offset}
                """.format(where=self.where(
            self.setup_filter(year_built, city, params)),
                           limit=size,
                           offset=offset)
        result = self.conn.execute(text(query))
        return result.fetchall()
