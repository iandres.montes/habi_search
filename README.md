# Servicio de consulta

Habi desea tener dos microservicios, dentro de los que esta este en especial, un servicio desarrollado con python y con una guia de estilos definida como PEP8 para python. Las funciones princiales del servicio son los siguientes:

- 1, Los usuarios pueden consultar los inmuebles con los estados: “pre_venta”, “en_venta” y “vendido” (los inmuebles con estados distintos nunca deben ser visibles por el usuario).
- 2, Los usuarios pueden filtrar estos inmuebles por: Año de construcción, Ciudad, Estado.
- 3, Los usuarios pueden aplicar varios filtros en la misma consulta.
- 4, Los usuarios pueden ver la siguiente información del inmueble: Dirección, Ciudad,
Estado, Precio de venta y Descripción.

Este punto debe ofrecer la funcionalidad de consulta y filtrado total de los inmuebles guardados en la base de datos, estos registros se encuentran en la tabla "property", los filtros principales que pueden hacer los usuarios externos de la api son:

	3, pre_venta
	4, en_venta
	5, vendido

Los ID's para cada estado es el mismo orden en esa lista, el usuario podra filtrar cada uno de esos estados realizando una peticion tipo GET y utilizando parametros en la url para realizar filtros especificos y no enviar ninguno para recibir todos los estados en el mismo orden de la fila.

Ademas, los usuarios tambien pueden realizar consultas por 
* Año de construccion
* Ciudad
* Estado

# TECNOLOGIA A UTILIZAR:
Se dejo como requerimiento tambien no utilizar ningun framework, sin embargo, con la cantidad de dias a entregar, se utilizara FastApi (python) porque nos permetira la autodocumentacion y las pruebas unitarias de la aplicacion y se utilizara un conector como sqlalchemy para la base de datos, sin embargo, no se utilizara las consultas del ORM, utilizaremos consultas compuestas con SQL scripts.

Lo primero que haremos sera realizar el repositorio, y el readme con esta misma informacion de inicio, seguido de eso, dejaremos todo el codigo en un unico archivo para facilitar su lectura. 

La informacion que la Api debe retornar en formato JSON es la siguiente:

```
{
  "direccion": string,
  "ciudad": string,
  "estado": {
    "id": integer,
    "label": string
  },
  "precio_venta": float,
  "descripcion": string
}
```

# Resolucion del problema
A continuacion detallare los retos o las dudas que fueron surgiendo mientras desarrollaba el codigo.

* Se pidio que no se utilizaran frameworks, por lo que intui que se referian a ORM, aun asi, limite el uso de frameworks a la conexion a base de datos y al servicio HTTP
* No conocia PEP8-P para python, luego de estudiar y conseguir varias opciones de como aplicar, decidi utilizar yapf, un autoformateador de python con PEP8

## ¿que framework utilizar para el servicio? (Django, Flask, FastApi)
El framework que mas he utilizado es flask, por el numero de modulos a integrar, la facilidad de uso y lo facil que es moldearlo a lo que se necesita, sin embargo, al leer los requerimientos de la prueba, note que algo en comun que esta en los requerimiento, sobretodo en el segundo punto, es Velocidad.
Entonces, sera mejor utilizar un framework que se esta haciendo porpular por la similitud en tiempo de respuesta con Go y Java, y fue asi como decidi utilizar FastApi. 

## ¿Es necesario usar schemas para las respuesta? 
Al igual que React, python esta permitiendo en sus nuevas versiones el uso de la definicion de tipos de variables o parametros, por lo que no sabia si seria totalmente necesario, sin embargo, luego de investigar solo un poco, llegue a la conclusion de que era lo mejor, la respuesta es un poco mas rapido en comparacion con la version sin schemas.

## ¿Debe estar todo en un solo archivo?
Al principio, pensando que iba a ser lo mejor, coloque todo el codigo base en el archivo main.py, pero luego de meditarlo un poco, decidi dividir el codigo de una manera mucho mas intuitiva y organizada, esto ayuda a la comprension del codigo y a un soporte mucho mas eficiente y rapido. 

# RECOMENDACIONES
- Filtro ciudad: En el caso del filtro del endpoint "/property" seria recomendable tener en la base de datos una tabla en la que se tenga una lista de las ciudades en las que funciona la plataforma, de esa forma, seria mas rapido y mejor realizar un filtro por id's

# DOCUMENTACION
A continuacion la documentacion del despliegue del servicio, de como realizar el test del endpoint principal y tambien como utilizar el endpoint:

## Comenzando 🚀
_El servicio fue contruido principalmente en Windows con FastApi y en un entorno virtual con Anaconda por lo que tiene requirements para pip y para conda.
_Este proyecto fue creado y testeado en un entorno virtual creado por **Anaconda**, en alternativa a este se puede utilizar **Virtualenv**, la version de python utilizada y recomendada es **Python 3.6**_

### Conda
En caso de que quieras ejecutar el programa en un entorno virtual con Anaconda, debes seguir la siguiente instruccion:

```
conda create -n <nombre_del_entorno>
```
luego activas el entorno ejecutando:

```
conda activate <nombre_del_entorno>
```

### Virtualenv

En caso de que quieras usar Virtualenv deberas seguir las siguientes instrucciones:
En este caso asumiremos que estas usando un entorno con base Linux (ubuntu en su version 20), en la mayoria de distribuciones Linux tiene preinstalado Python 2 pero no es la version que necesitamos, para ello necesitamos instalar python3 y sus dependencias principales:

```
apt install python3 python3-pip virtualenv
```
luego de ello procedemos a ubicar el directorio principal de python3:
```
which python3
```

este comando respondera algo como lo siguiente:
```
/usr/bin/python3
```
entonces procederemos a realizar el comando para crear el entorno:
```
virtualenv -p /usr/bin/python3 <nombre_del_entorno>
```
para finalizar, debemos iniciar el entorno, para eso lanzamos el siguiente comando:
```
source <nombre_del_entorno>/bin/activate
```

### Pre-requisitos 📋
_para utilizar el programa necesitas instalar las dependencias:_

```
fastapi==0.70.1
mysqlclient==2.1.0
pycodestyle==2.8.0
pydantic==1.8.2
pyparsing==3.0.6
pytest==6.2.5
requests==2.26.0
SQLAlchemy==1.4.29
typing_extensions==4.0.1
urllib3==1.26.7
uvicorn==0.16.0
```

para instalarlas solo necesitas ejecutar el siguiente comando:

```
pip install -r requirements.txt
```

## .env Configuraciones de entorno
hay un archivo llamado *.env* dentro podras configurar el acceso a base de datos.

## Despliegue 📦
Ahora ejecutaremos el comando para ponerlo en produccion, para ello debemos tener en cuanta el numero de asines a analizar y el numero de hilos a correr, por ejemplo la forma mas optima para 10000 asines (basandose en la prueba en el momento de desarrollo) es de la siguiente forma:

```
$ uvicorn main:app
```

## Uso de la Api
_El principal uso de la api es en el endpoint **/property/** la documentacion del endpoint la podra encontrar en la ruta **http:localhost:8000/docs** sin embargo, explicare como utilizar de forma sencilla el endpoint

_Para eso simplemente vamos a realizar una peticion get al endpoint con los filtros que queramos aplicar:

```
curl --location --request POST 'http://localhost:8000/property/?city=pereira,bogota&year=2020&status=pre_venta,vendido,en_venta&size=10&offset=0'
```

## Construido con 🛠️
_estas son las principales herramientas que se utilizaron en el proyecto_

- [FastApi](https://fastapi.tiangolo.com/) - FastAPI framework, high performance, easy to learn, fast to code, ready for production
- [SQLAlchemy](https://docs.sqlalchemy.org/en/13/)

## Autores ✒️
- Ivan Montes - Trabajo inicial y documentacion [iandres.montes](https://gitlab.com/iandres.montes)
