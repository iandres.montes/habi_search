import uvicorn
from fastapi import FastAPI
from fastapi.testclient import TestClient
from app.database import db
from app.schemas import ResponseProperty

# inicializacion de FastApi
app = FastAPI()
# inicializacion de helper de DB
db = db()
# inicializacion de TestClient para las pruebas unitarias
client = TestClient(app)


class Property:
    """
    Clase 'Property' para poder hacer control de los items
    de la tabla property, un objeto siempre facilitara
    la administracion de datos, la inicializacion de
    datos y tambien la utilizacion de otros recursos
    como parsear el precio.
    """
    def __init__(self, item: tuple) -> None:
        self.id: int = item[0]
        self.address: str = item[1]
        self.city: str = item[2]
        self.price: int = item[3]
        self.description: str = item[4]
        self.year_built: str = item[5]
        self.status: dict = {
            "id": str(item[6]),
            "name": item[7],
            "label": item[8],
            "update": str(item[9]),
        }


"""
Se declara el endpoint principal, se define
el modelo de respuesta (schemas) y tambien
se le asigna un nombre para poder ver la
documentacion swagger de una forma mas intuitiva.

Ademas de eso se declaran parametros para el
endpoint y el tipo de datos a recibir.
Definir el tipo de datos nos ahorra el tiempo
de tener que validar los datos que
recibimos por medio de los parametros
"""


@app.get("/property/", response_model=ResponseProperty, name="get property")
async def root(status: str = "*",
               year: int = None,
               city: str = None,
               size: int = 10,
               offset: int = 0) -> dict:
    """
    La funcionalidad princila de este endpoint
    es devolver la informacion de la tabla property
    de Habi, tambien esta la opcion de filtrar esta
    informacion, utilizando diferentes paranetros
    ya establecidos como:
    * city: filtra por el nombre de la ciudad
    (la recomendacion en el readme)
    * year: filtra por el año de construccion del
    inmueble
    * size: devuelve una lista del tamaño que el usuario final
    requiera o prefiera, por defecto es 10, es decir, devolvera
    un total de 10 inmuebles por defecto
    * offset: este se utiliza para la paginacion del front, por
    defecto es 0, si el usuario final quisiera ir a la pagina 2,
    entonces el offset seria size * 1, si quisiera
    ir a la pagina dos, el offset seria size * 2
    * status: este parametro realiza filtro por el estado
    de los inmuebles que pueden ser los siguientes:
        - pre_venta
        - en_venta
        - vendido
    En este punto utilizamos un for simple como un lambda
    para poder ahorrar espacio en codigo, sin embargo,
    gracias a la legibilidad del codigo y de los nombres
    de las funciones, logra ser facil de entender y administrar.

    Se creo un objeto, en el cual se espera recibir una tupla con los datos
    a procesar, como no se esta utilizando ningun orm, los datos vienen en
    una tupla, es decir, no es un objeto con el cual podamos trabajar de una
    forma mas sencilla y rapida.
    """
    retorno = [
        Property(x).__dict__ for x in db.filter(year_built=year,
                                                city=city,
                                                size=size,
                                                offset=offset,
                                                params=status)
    ]
    return {
        "error": False,
        "message": "Lista de casas y apartamento Habi",
        "total_rows": len(retorno),
        "response": retorno
    }


""" Test Client o Pruebas Unitarias Simples
Estas funciones tienen como proposito probar de diferentes formas los
endpoints que tenga el servicio.

Se estaran haciendo prruebas para el endpoint "/property/" de diferentes
formas:
"""


def test_read_main():
    """
    1, Successful Response (test_read_main): Esta funcion realiza una
    prueba GET esperando siempre una respuesta con codigo 200, siempre
    esperaremos este codigo de respuesta ya que aunque no hayan datos
    en la base de datos, esperamos que el servicio responda adecuadamente.
    """
    response = client.get(
        """/property/?
                city=pereira,bogota&
                year=2020&
                status=pre_venta,vendido,en_venta&
                size=10&
                offset=0"""
    )
    assert response.status_code == 200
    assert response.json()["error"] is False


def test_read_inexistent_item():
    """
    2, Not Found (test_read_inexistent_item): En este punto realizara
    una peticion GET a un endpoint inexistente, por lo que debe
    responder de forma correcta con un codigo
    de respuesta 404.
    """
    response = client.get("/property/sincelejo")
    assert response.status_code == 404
    assert response.json() == {"detail": "Not Found"}


def test_read_method_allowed():
    """
    3, Method Not Allowed (test_read_method_allowed): Esta funcion hara
    un metodo no permitido al endpoint, que en este caso es un metodo
    "post" el cual no esta declarado en el servicio.
    """
    response = client.post("/property/")
    assert response.status_code == 405
    assert response.json() == {"detail": "Method Not Allowed"}


if __name__ == "__main__":
    uvicorn.run(app, host="0.0.0.0", port=8000)
